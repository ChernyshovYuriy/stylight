package com.example.Stylight.business;

import com.example.Stylight.model.BoardData;
import com.example.Stylight.model.ItemData;
import com.example.Stylight.model.UserData;
import com.example.Stylight.utilities.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/26/13
 * Time: 11:18 AM
 */
public class JSONHelper {

    public static void getUserInfo(String jsonString, IJSONHelper aJSONHelperCallBack) {

        UserData userData = new UserData();

        JSONObject jsonObject;
        JSONObject creatorObject;
        try {
            jsonObject = new JSONObject(String.valueOf(jsonString));
            creatorObject = jsonObject.getJSONObject("board").getJSONObject("creator");

            userData.setUsername(creatorObject.getString("username"));
            userData.setAvatarURL(creatorObject.getString("picture"));

            aJSONHelperCallBack.onUserDataComplete(userData);
        } catch (JSONException e) {
            Logger.e("Get JSONObject error " + e);
            aJSONHelperCallBack.onProcessError();
        }
    }

    public static void getBoards(String jsonString, IJSONHelper aJSONHelperCallBack) {
        JSONObject jsonObject;
        JSONArray items;
        try {
            jsonObject = new JSONObject(String.valueOf(jsonString));
            items = jsonObject.getJSONArray("items");
            JSONObject object;
            LinkedList<BoardData> linkedList = new LinkedList<BoardData>();
            BoardData boardData;
            for (int i = 0; i < items.length(); i++) {
                object = items.getJSONObject(i);
                boardData = new BoardData();
                boardData.setJsonObject(object);
                linkedList.add(boardData);
                Logger.d("JSON Item: " + boardData.getJsonObject().toString());
            }
            Logger.d("JSON res: " + linkedList.size());
            aJSONHelperCallBack.onBoardsComplete(linkedList);
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.e("Get JSONObject error " + e);
            aJSONHelperCallBack.onProcessError();
        }
    }

    public static void getBoardItems(String jsonString, IJSONHelper aJSONHelperCallBack) {
        JSONObject jsonObject;
        JSONArray items;
        try {
            jsonObject = new JSONObject(String.valueOf(jsonString));
            items = jsonObject.getJSONObject("board").getJSONArray("items");
            LinkedList<ItemData> linkedList = new LinkedList<ItemData>();
            ItemData itemData;
            JSONObject object;
            JSONObject objectContent;
            JSONArray objectImages;
            for (int i = 0; i < items.length(); i++) {
                object = items.getJSONObject(i);
                // TODO: This is absolutely wrong approach. If value is absent - then passing null is incorrect!
                try {
                    objectContent = object.getJSONObject("content");
                } catch (JSONException e) {
                    objectContent = null;
                }
                Logger.d("Get Item: " + object);
                itemData = new ItemData();
                if (objectContent != null) {
                    itemData.setImagePath(objectContent.getString("image"));
                    itemData.setProduct(Boolean.FALSE);
                    itemData.setContent(Boolean.TRUE);

                    itemData.setLabelText(objectContent.getString("text"));
                } else {
                    objectImages = object.getJSONObject("product").getJSONArray("images");
                    if (objectImages != null) {
                        JSONObject objectImage;
                        for (int j = 0; j < objectImages.length(); j++) {
                            objectImage = objectImages.getJSONObject(j);
                            if (objectImage.getBoolean("primary")) {
                                itemData.setProduct(Boolean.TRUE);
                                itemData.setContent(Boolean.FALSE);
                                itemData.setImagePath(objectImage.getString("url"));
                                break;
                            }
                        }
                    }
                }
                itemData.setSizeX(object.getInt("sizeX"));
                itemData.setSizeY(object.getInt("sizeY"));
                itemData.setPositionX(object.getInt("positionX"));
                itemData.setPositionY(object.getInt("positionY"));
                linkedList.add(itemData);
            }
            aJSONHelperCallBack.onBoardItemsComplete(linkedList);

        } catch (JSONException e) {
            e.printStackTrace();
            Logger.e("Get JSONObject error " + e);
            aJSONHelperCallBack.onProcessError();
        }
    }
}