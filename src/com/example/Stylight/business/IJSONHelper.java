package com.example.Stylight.business;

import com.example.Stylight.model.BoardData;
import com.example.Stylight.model.ItemData;
import com.example.Stylight.model.UserData;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/26/13
 * Time: 2:09 PM
 */
public interface IJSONHelper {
    void onUserDataComplete(UserData userData);
    void onBoardItemsComplete(Collection<ItemData> collection);
    void onBoardsComplete(Collection<BoardData> collection);
    void onProcessError();
}