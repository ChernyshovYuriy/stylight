package com.example.Stylight;

import android.app.Application;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import com.example.Stylight.network.ConnectivityReceiver;
import com.example.Stylight.network.IConnectivityEvents;
import com.example.Stylight.utilities.Logger;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/24/13
 * Time: 10:30 PM
 */
public class MainApplication extends Application implements IConnectivityEvents {

    public static float DENSITY = DisplayMetrics.DENSITY_DEFAULT;
    public static float RESDENSITY = 0;

    private final ConnectivityReceiver mConnectivityReceiver = new ConnectivityReceiver(this);
    private boolean mConnectivityAvailable = false;

    private static volatile MainApplication mInstance = null;
    //ui events proxy handler
    private final Handler mUIHandler = new Handler(Looper.getMainLooper());

    public MainApplication() {
        super();
        mInstance = this;
    }

    public static MainApplication getInstance() {
        if (mInstance == null) {
            synchronized (MainApplication.class) {
                if (mInstance == null) {
                    new MainApplication();
                }
            }
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.i("------------ Create Application ------------");

        initConstants();
        startConnectivityReceiver();
    }

    @Override
    public void onConnectivityChanged(boolean available) {
        mConnectivityAvailable = available;
    }

    private void startConnectivityReceiver() {
        try {
            Logger.i("Registering ConnectivityReceiver");
            registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (Throwable th) {
            Logger.e("Can't register mConnectivityReceiver", th);
        }
        requestConnectivityStatus();
    }

    public void stopConnectivityReceiver() {
        try {
            Logger.i("Unregistering ConnectivityReceiver");
            unregisterReceiver(mConnectivityReceiver);
        } catch (IllegalArgumentException e) {
            Logger.d("ConnectivityReceiver actually is not registered, ignoring");
        }
    }

    public boolean isConnectivityAvailable() {
        return mConnectivityAvailable;
    }

    public void runInUIThread(Runnable r) {
        mUIHandler.post(r);
    }

    public void runInUIThread(Runnable r, long delayMS) {
        mUIHandler.postDelayed(r, delayMS);
    }

    public static boolean isRunningUIThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public File getExternalFilesDirAPI8(String type) {
        return super.getExternalFilesDir(type);
    }

    private void requestConnectivityStatus() {
        mConnectivityReceiver.requestConnectivityStatus();
    }

    private void initConstants() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        DENSITY = displayMetrics.densityDpi;
        RESDENSITY = displayMetrics.density;
    }
}