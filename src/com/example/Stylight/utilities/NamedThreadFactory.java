package com.example.Stylight.utilities;

import java.util.concurrent.ThreadFactory;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 1/28/13
 */
public class NamedThreadFactory implements ThreadFactory {

    private final String mThreadsNamePrefix;
    private int mThreadsCounter;
    private boolean mMaxPriority;

    public NamedThreadFactory(String threadsNamePrefix) {
        mThreadsNamePrefix = threadsNamePrefix;
        mThreadsCounter = 0;
    }

    public NamedThreadFactory(String threadsNamePrefix, boolean maxPriority) {
        mThreadsNamePrefix = threadsNamePrefix;
        mThreadsCounter = 0;
        mMaxPriority = maxPriority;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        mThreadsCounter++;
        Thread thread = new Thread(runnable, mThreadsNamePrefix + "-t#" + mThreadsCounter);
        if (mMaxPriority) {
            thread.setPriority(Thread.MAX_PRIORITY);
        }
        return thread;
    }
}