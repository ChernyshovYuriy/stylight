package com.example.Stylight.utilities;

import android.content.Context;
import android.util.Log;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

import java.io.*;

public class Logger {

    public static final String LOG_TAG = "STYLIGHT";
	public static final String ERROR_LOG_PREFIX = "LOG_ERR: ";
	private static final String LOG_FILENAME = "Stylight.log";
	private static final int MAX_BACKUP_INDEX = 10;
	private static final String MAX_FILE_SIZE = "750KB";

    private static String initLogsDirectory;
	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);

	//TODO
	//Implement LogCat appender
	//Need to be called from main_layout activity onCreate;
	//Implement Properties from File
	public static void initLogger(Context context) {
        initLogsDirectories(context);
		String fileName = getCurrentLogsDirectory() + "/" + LOG_FILENAME;
		logger.setLevel(Level.DEBUG);
		org.apache.log4j.Layout layout = new PatternLayout("%d [%t] %-5p %m%n");
        try {
            logger.removeAllAppenders();
        } catch (Exception e) {
            Logger.e("Unable to remove logger uppenders.");
        }
		try {
			RollingFileAppender rollingFileAppender = new RollingFileAppender(layout, fileName);
			rollingFileAppender.setMaxFileSize(MAX_FILE_SIZE);
			rollingFileAppender.setMaxBackupIndex(MAX_BACKUP_INDEX);
			logger.addAppender(rollingFileAppender);
		} catch(IOException ioe) {
			Log.e("STYLIGHT", "unable to create log file: " + fileName);
		}
        Logger.d("Current log stored to " + fileName);
	}

    private static void initLogsDirectories(Context context) {
        initLogsDirectory = context.getFilesDir() + "/logs";
    }
        
    public static String getCurrentLogsDirectory() {
        if (ApplicationUtilities.externalStorageAvailable()) {
            String extLogsDirectory = ApplicationUtilities.getExternalStorageDir();
            if (extLogsDirectory != null && !extLogsDirectory.equals("")) {
                return extLogsDirectory + "/logs";
            }
        }
        return initLogsDirectory;
    }
		
	public static void e(String logMsg) {
        e(logMsg, (Throwable) null);
	}

	public static void w(String logMsg) {
        w(logMsg, (Throwable) null);
	}

	public static void i(String logMsg) {
        i(logMsg, (Throwable) null);
	}

	public static void d(String logMsg) {
        d(logMsg, (Throwable) null);
	}

    public static void e(String logPrefix, String logMsg) {
    	e(logPrefix + logMsg);
    }

    public static void w(String logPrefix, String logMsg) {
    	w(logPrefix + logMsg);
    }

    public static void i(String logPrefix, String logMsg) {
    	i(logPrefix + logMsg);
    }

    public static void d(String logPrefix, String logMsg) {
    	d(logPrefix + logMsg);
    }

    public static void e(String logMsg, Throwable t) {
        logMsg = ERROR_LOG_PREFIX + logMsg;
        if (t != null) {
            logger.error(logMsg, t);
            Log.e(LOG_TAG, logMsg, t);
        } else {
            logger.error(logMsg);
            Log.e(LOG_TAG, logMsg);
        }
    }

    public static void w(String logMsg, Throwable t) {
        if (t != null) {
            Log.w(LOG_TAG, logMsg, t);
        } else {
            Log.w(LOG_TAG, logMsg);
        }
    }

    public static void i(String logMsg, Throwable t) {
        if (t != null) {
            logger.info(logMsg, t);
            Log.i(LOG_TAG, logMsg, t);
        } else {
            logger.info(logMsg);
            Log.i(LOG_TAG, logMsg);
        }
    }

    public static void d(String logMsg, Throwable t) {
        if (t != null) {
            //logger.debug(logMsg, t);
            Log.d(LOG_TAG, logMsg, t);
        } else {
            //logger.debug(logMsg);
            Log.d(LOG_TAG, logMsg);
        }
    }
}