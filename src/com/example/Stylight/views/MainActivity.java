package com.example.Stylight.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.Stylight.R;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button boardViewButton = (Button) findViewById(R.id.question_answer_btn);
        boardViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, QuestionAnswerActivity.class);
                startActivity(intent);
            }
        });

        Button styleButton = (Button) findViewById(R.id.button_activity_btn);
        styleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ButtonViewActivity.class);
                startActivity(intent);
            }
        });

        Button fetchBoardButton = (Button) findViewById(R.id.fetch_board_activity_btn);
        fetchBoardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FetchBoardActivity.class);
                startActivity(intent);
            }
        });
    }
}