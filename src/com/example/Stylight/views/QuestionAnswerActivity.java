package com.example.Stylight.views;

import android.app.Activity;
import android.os.Bundle;
import com.example.Stylight.R;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 28.05.13
 * Time: 10:23
 */
public class QuestionAnswerActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_answer_layout);
    }
}