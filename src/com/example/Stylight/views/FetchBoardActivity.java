package com.example.Stylight.views;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.Stylight.MainApplication;
import com.example.Stylight.R;
import com.example.Stylight.business.IJSONHelper;
import com.example.Stylight.business.JSONHelper;
import com.example.Stylight.business.bitmap.ImageCache;
import com.example.Stylight.business.bitmap.ImageFetcher;
import com.example.Stylight.components.BoardItemView;
import com.example.Stylight.components.SafeToast;
import com.example.Stylight.model.BoardData;
import com.example.Stylight.model.ItemData;
import com.example.Stylight.model.UserData;
import com.example.Stylight.network.HTTPHelper;
import com.example.Stylight.network.IHTTPHelper;
import com.example.Stylight.utilities.ApplicationConstants;
import com.example.Stylight.utilities.Logger;
import com.example.Stylight.utilities.NamedThreadFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/25/13
 * Time: 5:38 PM
 */
public class FetchBoardActivity extends FragmentActivity implements IJSONHelper, IHTTPHelper {

    /*
    * http://developer.android.com/training/displaying-bitmaps/index.html
     */
    private ImageFetcher mImageFetcher;
    private static final String IMAGE_CACHE_DIR = "images";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetch_board_layout);

        // Fetch screen height and width, to use as our max size when loading images as this activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the image scaling ensures the
        // image is larger than this, we should be left with a resolution that is appropriate for both portrait and
        // landscape.
        final int longest = (height > width ? height : width);

        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        // Set memory cache to 25% of app memory
        cacheParams.setMemCacheSizePercent(0.25f);

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(Boolean.TRUE);
    }

    @Override
    public void onResume() {
        super.onResume();

        mImageFetcher.setExitTasksEarly(false);

        ExecutorService executorService = Executors.newSingleThreadExecutor(new NamedThreadFactory("fetch-board"));
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (!MainApplication.getInstance().isConnectivityAvailable()) {
                    SafeToast.showToastAnyThread(getString(R.string.network_not_available));
                    return;
                }
                String secureCookie = getSecureCookie();
                Logger.d("Secure Cookie: " + secureCookie);
                if (secureCookie.equals("")) {
                    hideProgress();
                    SafeToast.showToastAnyThread("Can not fetch board ...");
                    return;
                }
                String boardData = fetchData(secureCookie);
                hideProgress();
                Logger.d("Board data: " + boardData);
                if (!boardData.equals("")) {
                    //JSONHelper.getUserInfo(boardData, FetchBoardActivity.this);
                    //JSONHelper.getBoardItems(boardData, FetchBoardActivity.this);
                    JSONHelper.getBoards(boardData, FetchBoardActivity.this);
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    @Override
    public void onUserDataComplete(final UserData userData) {
        MainApplication.getInstance().runInUIThread(new Runnable() {
            @Override
            public void run() {
                TextView userNameLabel = (TextView) findViewById(R.id.user_name_view);
                userNameLabel.setText(userData.getUsername());

                ImageView mImageView = (ImageView) findViewById(R.id.used_avatar_view);
                mImageFetcher.loadImage(userData.getAvatarURL(), mImageView);
            }
        });

    }

    @Override
    public void onBoardsComplete(Collection<BoardData> collection) {
        Iterator<BoardData> boardDataIterator = collection.iterator();
        Logger.d("Board init: " + collection.size());
        BoardData boardData;
        int counter = 0;
        while (boardDataIterator.hasNext()) {
            boardData = boardDataIterator.next();
            if (boardData != null) {
                Logger.d("Board: " + boardData.getJsonObject().toString());
                if (counter++ == 1) {
                    JSONHelper.getUserInfo(boardData.getJsonObject().toString(), FetchBoardActivity.this);
                }
                JSONHelper.getBoardItems(boardData.getJsonObject().toString(), FetchBoardActivity.this);
            }
        }
    }

    @Override
    public void onBoardItemsComplete(Collection<ItemData> collection) {
        // Fetch screen height and width, to use as our max size when loading images as this activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int width = displayMetrics.widthPixels;

        final int sizeH = width / 4;
        final double sizeV = 1.34 * sizeH;
        //final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.board_data_relative_layout);

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.board_data_linear_layout);
        final RelativeLayout relativeLayout = new RelativeLayout(this);

        final LinearLayout separatorView = new LinearLayout(this);
        LinearLayout.LayoutParams separatorLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 5);
        separatorView.setLayoutParams(separatorLayoutParams);
        separatorView.setBackgroundColor(Color.RED);

        final Iterator<ItemData> iterator = collection.iterator();
        MainApplication.getInstance().runInUIThread(new Runnable() {
            @Override
            public void run() {
                ItemData itemData;
                int padding = (int) (2 * MainApplication.RESDENSITY);
                BoardItemView boardItemView;
                RelativeLayout.LayoutParams layoutParams;
                while (iterator.hasNext()) {
                    itemData = iterator.next();
                    Logger.d("Load Image " + itemData.getImagePath());
                    layoutParams = new RelativeLayout.LayoutParams(sizeH * itemData.getSizeX(),
                            (int) (sizeV * itemData.getSizeY()));
                    layoutParams.leftMargin = itemData.getPositionX() * sizeH;
                    layoutParams.topMargin = (int) (itemData.getPositionY() * sizeV);

                    boardItemView = new BoardItemView(FetchBoardActivity.this);
                    boardItemView.init();
                    relativeLayout.addView(boardItemView, layoutParams);
                    boardItemView.setPadding(padding, padding, padding, padding);

                    if (itemData.isProduct()) {
                        boardItemView.setProductView();
                    } else {
                        boardItemView.getImageView().setScaleType(ImageView.ScaleType.FIT_XY);
                    }
                    if (!itemData.getImagePath().equals("")) {
                        mImageFetcher.loadImage(itemData.getImagePath(), boardItemView.getImageView());
                    } else {
                        boardItemView.setProductView();
                        boardItemView.setLabel(itemData.getLabelText());
                    }
                }

                linearLayout.addView(relativeLayout);
                linearLayout.addView(separatorView);
            }
        });
    }

    @Override
    public void onProcessError() {
        hideProgress();
        SafeToast.showToastAnyThread("Can not fetch board data ...");
    }

    @Override
    public void onRequestError() {
        hideProgress();
        SafeToast.showToastAnyThread("Can not fetch board data ...");
    }

    private String getSecureCookie() {
        HttpResponse httpResponse = HTTPHelper.makeRequest(ApplicationConstants.API_LOGIN_URL, this);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        if (responseCode == 200) {
            Header[] headers = httpResponse.getHeaders("Set-Cookie");
            for (Header header : headers) {
                if (header.getValue().startsWith("st_secure=")) {
                    return header.getValue().substring(10, header.getValue().indexOf(";"));
                }
            }
        }
        return "";
    }

    private String fetchData(String secureCookie) {
        HttpResponse httpResponse = HTTPHelper.makeRequest(ApplicationConstants.API_BOARD_URL, this, secureCookie);
        Logger.d("Call response code: " + httpResponse.getStatusLine().getStatusCode());
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        if (responseCode == 200) {
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                try {
                    return EntityUtils.toString(entity);
                } catch (IOException e) {
                    Logger.e("Call response error: " + e);
                }
            }
        }
        return "";
    }

    private void hideProgress() {
        MainApplication.getInstance().runInUIThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_view);
                progressBar.setVisibility(View.GONE);

                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.user_info_layout);
                linearLayout.setVisibility(View.VISIBLE);

                ScrollView scrollView = (ScrollView) findViewById(R.id.board_data_layout);
                scrollView.setVisibility(View.VISIBLE);
            }
        });
    }
}