package com.example.Stylight.views;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import com.example.Stylight.MainApplication;
import com.example.Stylight.R;
import com.example.Stylight.components.StyledButtonImage;
import com.example.Stylight.components.StyledButtonBackground;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/25/13
 * Time: 5:26 PM
 */
public class ButtonViewActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button_view_layout);

        processButton();
    }

    private void processButton() {
        final StyledButtonImage heartShape = new StyledButtonImage();
        heartShape.setScaleFactor(20 * MainApplication.RESDENSITY);
        heartShape.setFillColor(Color.WHITE);

        final StyledButtonBackground buttonBackground = new StyledButtonBackground();
        final int[] backgroundGradientColors = new int[]{getResources().getColor(R.color.gradient_start_color),
                getResources().getColor(R.color.gradient_end_color)};
        final int[] backgroundGradientSelectedColors = new int[]{getResources().getColor(R.color.gradient_selected_start_color),
                getResources().getColor(R.color.gradient_selected_end_color)};
        final int[] backgroundTopLineColors = new int[]{getResources().getColor(R.color.top_shadow_color),
                getResources().getColor(R.color.top_shadow_fade_color)};
        final int[] backgroundTopLinePressedColors = new int[]{getResources().getColor(R.color.top_shadow_pressed_color),
                getResources().getColor(R.color.top_shadow_pressed_fade_color)};

        buttonBackground.setGradientColors(backgroundGradientColors);
        buttonBackground.setCornerRadius(10 * MainApplication.RESDENSITY);
        buttonBackground.setTopLineColors(backgroundTopLineColors);

        final ImageButton button = (ImageButton) findViewById(R.id.main_btn);
        button.setImageDrawable(heartShape);
        button.setBackgroundDrawable(buttonBackground);
        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    heartShape.setFillColor(Color.WHITE);
                    buttonBackground.setGradientColors(backgroundGradientColors);
                    buttonBackground.setTopLineColors(backgroundTopLineColors);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    heartShape.setFillColor(getResources().getColor(R.color.heart_on_press_color));
                    buttonBackground.setGradientColors(backgroundGradientSelectedColors);
                    buttonBackground.setTopLineColors(backgroundTopLinePressedColors);
                }
                return Boolean.FALSE;
            }
        });
    }
}