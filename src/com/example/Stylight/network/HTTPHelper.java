package com.example.Stylight.network;

import com.example.Stylight.utilities.Logger;
import org.apache.http.HttpResponse;

import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/12/13
 */
public class HTTPHelper {

    public static HttpResponse makeRequest(final String uri, IHTTPHelper aHelperCallBack) {
        return makeRequest(uri, aHelperCallBack, null);
    }

    public static HttpResponse makeRequest(final String uri, IHTTPHelper aHelperCallBack, String aSecureCookie) {
        Logger.d("HTTPHelper request to " + uri);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Callable<HttpResponse> worker = new HttpRequestCallable(uri, aSecureCookie);
        Future<HttpResponse> submit = executor.submit(worker);
        try {
            HttpResponse httpResponse = submit.get();
            if (httpResponse == null) {
                aHelperCallBack.onRequestError();
            }
            return httpResponse;
        } catch (InterruptedException e) {
            Logger.e("HTTPHelper makeRequest InterruptedException " + e);
        } catch (ExecutionException e) {
            Logger.e("HTTPHelper makeRequest ExecutionException " + e);
        }
        aHelperCallBack.onRequestError();
        return null;
    }
}