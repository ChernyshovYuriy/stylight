package com.example.Stylight.network;

import com.example.Stylight.utilities.ApplicationConstants;
import com.example.Stylight.utilities.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/12/13
 */
public class HttpRequestCallable implements Callable<HttpResponse> {

    private String mUrl;
    private String mSecureCookie;

    public HttpRequestCallable(String aUrl, String aSecureCookie) {
        mSecureCookie = aSecureCookie;
        mUrl = aUrl;
    }

    @Override
    public HttpResponse call() throws Exception {
        HttpGet request = new HttpGet(mUrl);

        try {
            request.setHeader("X-apiKey", ApplicationConstants.X_API_KEY);
            request.setHeader("Accept-Language", "de-DE");
            if (mSecureCookie != null && !mSecureCookie.equals("")) {
                request.setHeader("Cookie", "st_secure=" + mSecureCookie);
            }
            HttpClient httpClient = new DefaultHttpClient();
            return httpClient.execute(request);

        } catch (UnsupportedEncodingException e) {
            Logger.e("HttpRequestCallable Unsupported encoding in data " + e);
        } catch (ClientProtocolException e) {
            Logger.e("HttpRequestCallable Protocol based error " + e);
        } catch (IOException e) {
            Logger.e("HttpRequestCallable IOException " + e);
        }
        return null;
    }
}