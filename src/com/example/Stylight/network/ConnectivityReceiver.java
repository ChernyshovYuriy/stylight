package com.example.Stylight.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.Stylight.MainApplication;
import com.example.Stylight.utilities.Logger;

import java.util.Arrays;

public class ConnectivityReceiver extends BroadcastReceiver {
	
	public static final int CONNECTIVITY_STATE_UNKNOWN = 0;
	public static final int CONNECTIVITY_STATE_CONNECTED = 1;
	public static final int CONNECTIVITY_STATE_CONNECTING = 2;
	public static final int CONNECTIVITY_STATE_DISCONNECTED = 3;
	public static final int CONNECTIVITY_STATE_DISCONNECTEDING = 4;
	public static final int CONNECTIVITY_STATE_SUSPENDED = 5;
	
	public static int mState = CONNECTIVITY_STATE_UNKNOWN;

	private final IConnectivityEvents mCallback;
	
	private boolean mIsWiFiConnection;
	
	public ConnectivityReceiver(IConnectivityEvents callback) {
        mCallback = callback;
	}
	
	public boolean isWiFiConnection() {
		return mIsWiFiConnection;
	}

    public void requestConnectivityStatus() {
        onReceive(MainApplication.getInstance(), new Intent());
    }
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (context == null) {
			return;
		}

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = null;
        try {
            activeNetInfo = connectivityManager.getActiveNetworkInfo();
        } catch (Exception e){
            Logger.e("Unable to get NetworkInfo " + e);
        }

		boolean isAnyAvailable = false;
		if (activeNetInfo == null) {		
			NetworkInfo networkInfo[] = connectivityManager.getAllNetworkInfo();
			Logger.d("ConnectivityReceiver networkInfo: " + Arrays.toString(networkInfo));
			if (networkInfo != null && networkInfo.length > 0) {
				Logger.d("ConnectivityReceiver networkInfo length: " + networkInfo.length);
                for (NetworkInfo netInfo : networkInfo) {
                    Logger.d("ConnectivityReceiver netInfo: " + netInfo.isAvailable() + ", " + netInfo.isConnectedOrConnecting());
                    if (netInfo.isAvailable() && netInfo.isConnectedOrConnecting()) {
                        Logger.i("Alternative network, type: " + netInfo.getTypeName());
                        isAnyAvailable = true;
                        break;
                    }
                }
			}
		} else {
            isAnyAvailable = activeNetInfo.isAvailable();
            mIsWiFiConnection = activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI;
            int oldState = mState;
            // mState field is changed in following method
            String netState = getNetworkStateString(activeNetInfo.getState());
            if (mState != oldState) {
                Logger.i("Network conf changed, type: " + activeNetInfo.getTypeName() + ", state: " + netState);
            }
		}
		
        Logger.d("ConnectivityReceiver, isAnyAvailable: " + isAnyAvailable);
		if (!isAnyAvailable) {
			mState = CONNECTIVITY_STATE_DISCONNECTED;
		}

        mCallback.onConnectivityChanged(isAnyAvailable);
	}
	
	private static String getNetworkStateString(NetworkInfo.State state) {
        switch (state) {
            case CONNECTED:
            	mState = CONNECTIVITY_STATE_CONNECTED;
                return "Connected";
            case CONNECTING:
            	mState = CONNECTIVITY_STATE_CONNECTING;
                return "Connecting";
            case DISCONNECTED:
            	mState = CONNECTIVITY_STATE_DISCONNECTED;
                return "Disconnected";
            case DISCONNECTING:
            	mState = CONNECTIVITY_STATE_DISCONNECTEDING;
                return "Disconnecting";
            case SUSPENDED:
            	mState = CONNECTIVITY_STATE_SUSPENDED;
                return "Suspended";
        }
        mState = CONNECTIVITY_STATE_UNKNOWN;
        return " Unknown";
    }
}
