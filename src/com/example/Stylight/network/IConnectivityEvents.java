package com.example.Stylight.network;

/**
 * User: chernyshovyuriy
 * Date: 15.11.12
 */
public interface IConnectivityEvents {
    public void onConnectivityChanged(boolean available);
}