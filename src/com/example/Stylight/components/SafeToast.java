package com.example.Stylight.components;

import android.content.Context;
import android.widget.Toast;
import com.example.Stylight.MainApplication;
import com.example.Stylight.utilities.Logger;

/**
 * Date: 06.11.12
 * Time: 15:57
 */
public class SafeToast {

    public static void showToastAnyThread(CharSequence text) {
        showToastAnyThread(MainApplication.getInstance(), text);
    }

    public static void showToastAnyThread(final Context context, final CharSequence text) {
        if (MainApplication.isRunningUIThread()) {
            // we are already in UI thread, it's safe to show Toast
            showToastUIThread(context, text);
        } else {
            // we are NOT in UI thread, so scheduling task in handler
            MainApplication.getInstance().runInUIThread(new Runnable() {
                @Override
                public void run() {
                    showToastUIThread(context, text);
                }
            });
        }
    }

    private static void showToastUIThread(Context context, CharSequence text) {
        if (context == null) {
            context = MainApplication.getInstance();
        }
        Logger.i("Show toast: " + text);
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }
}