package com.example.Stylight.components;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.shapes.Shape;
import com.example.Stylight.MainApplication;
import com.example.Stylight.R;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 24.05.13
 * Time: 14:00
 */
public class HeartShape extends Shape {

    private double scaleFactor;
    private int fillColor;
    private Path path;

    public void setFillColor(int fillColor) {
        this.fillColor = fillColor;
    }

    public void setScaleFactor(double scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public HeartShape() {
        path = new Path();
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(1);
        paint.setAntiAlias(Boolean.TRUE);
        paint.setColor(fillColor);
        paint.setShadowLayer(2.0f, -7, 0, MainApplication.getInstance().getResources().getColor(R.color.heart_shadow_color));

        double angle = 0;
        int stepAngle = 360;
        float[] curPoints;
        double stepValue = (2 * Math.PI) / stepAngle;

        path.moveTo(0, 0);

        for (int i = 0; i <= stepAngle; i++) {
            curPoints = redraw(angle);
            path.lineTo(curPoints[0], curPoints[1]);
            angle += stepValue;
        }

        canvas.drawPath(path, paint);
    }

    private float[] redraw(double radians) {
        double radius = getRadius(radians);
        radius = radius * scaleFactor;
        float xPos = (float) (Math.sin(radians) * radius);
        float yPos = (float) (Math.cos(radians) * radius);
        return new float[]{xPos, yPos};
    }

    private double getRadius(double t) {
        double value_A = Math.sin(t) * Math.sqrt(Math.abs(Math.cos(t)));
        double value_B = (Math.sin(t) + 1.4);
        return value_A / value_B - 2 * Math.sin(t) + 2;
    }
}