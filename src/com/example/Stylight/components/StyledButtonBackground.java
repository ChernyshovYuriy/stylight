package com.example.Stylight.components;

import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import com.example.Stylight.MainApplication;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/25/13
 * Time: 8:39 AM
 */
public class StyledButtonBackground extends Drawable {

    private int[] gradientColors;
    private int[] topLineColors;
    private float cornerRadius;
    private BackgroundShape roundRectShape;
    private BackgroundShape roundRectShadowShape;
    private BackgroundShape roundRectTopLineShape;
    private ShapeDrawable rectangleDrawable;
    private ShapeDrawable rectangleShadowDrawable;
    private ShapeDrawable rectangleTopLineDrawable;

    public StyledButtonBackground() {
        roundRectShape = new BackgroundShape();
        roundRectShadowShape = new BackgroundShape();
        roundRectTopLineShape = new BackgroundShape();
        rectangleDrawable = new ShapeDrawable(roundRectShape);
        rectangleShadowDrawable = new ShapeDrawable(roundRectShadowShape);
        rectangleTopLineDrawable = new ShapeDrawable(roundRectTopLineShape);

        roundRectShadowShape.setIsShadow(Boolean.TRUE);
        roundRectShadowShape.setShadowColor(Color.GRAY);
    }

    public void setTopLineColors(int[] topLineColors) {
        this.topLineColors = topLineColors;
        invalidateSelf();
    }

    public void setGradientColors(int[] gradientColors) {
        this.gradientColors = gradientColors;
        invalidateSelf();
    }

    public void setCornerRadius(float cornerRadius) {
        this.cornerRadius = cornerRadius;
    }

    @Override
    public void draw(Canvas canvas) {

        int height = getBounds().height();
        int width = getBounds().width();

        roundRectShape.setGradientColors(gradientColors);
        roundRectShape.setCornerRadius(cornerRadius);

        roundRectShadowShape.setGradientColors(gradientColors);
        roundRectShadowShape.setCornerRadius(cornerRadius);

        roundRectTopLineShape.setLineHeight(5 * MainApplication.RESDENSITY);
        roundRectTopLineShape.setGradientColors(topLineColors);
        roundRectTopLineShape.setCornerRadius(cornerRadius);

        rectangleDrawable.setBounds(0, 0, width, height);
        rectangleShadowDrawable.setBounds(0, 0, width, height);
        rectangleTopLineDrawable.setBounds(0, 0, width, height);

        rectangleShadowDrawable.draw(canvas);
        rectangleDrawable.draw(canvas);
        rectangleTopLineDrawable.draw(canvas);
    }

    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 255;
    }
}