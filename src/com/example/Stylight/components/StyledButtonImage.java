package com.example.Stylight.components;

import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/23/13
 * Time: 10:38 PM
 */
public class StyledButtonImage extends Drawable {

    private double scaleFactor;
    private int fillColor;
    private HeartShape shape;
    ShapeDrawable shapeDrawable;

    public void setFillColor(int fillColor) {
        this.fillColor = fillColor;
        invalidateSelf();
    }

    public void setScaleFactor(double scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public StyledButtonImage() {
        shape = new HeartShape();
        shapeDrawable = new ShapeDrawable(shape);
    }

    @Override
    public void draw(Canvas canvas) {

        int height = getBounds().height();
        int width = getBounds().width();

        shape.setScaleFactor(scaleFactor);
        shape.setFillColor(fillColor);

        //Saving the canvas and later restoring it so only this image will be rotated.
        canvas.save(Canvas.MATRIX_SAVE_FLAG);
        canvas.rotate(-90);

        shapeDrawable.setBounds(-(height / 4), width - (width / 2), width, height);
        shapeDrawable.draw(canvas);

        canvas.restore();
    }

    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 255;
    }
}