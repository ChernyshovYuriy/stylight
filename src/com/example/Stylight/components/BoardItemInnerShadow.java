package com.example.Stylight.components;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import com.example.Stylight.MainApplication;
import com.example.Stylight.R;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/27/13
 * Time: 7:49 PM
 */
public class BoardItemInnerShadow extends Drawable {

    private BackgroundShape rectLeftLineShape;
    private BackgroundShape rectTopLineShape;
    private BackgroundShape rectRightLineShape;
    private BackgroundShape rectBottomLineShape;
    private ShapeDrawable rectLeftLineDrawable;
    private ShapeDrawable rectTopLineDrawable;
    private ShapeDrawable rectRightLineDrawable;
    private ShapeDrawable rectBottomLineDrawable;
    private int[] lineColors;

    public BoardItemInnerShadow() {
        super();
    }

    public void init() {
        rectLeftLineShape = new BackgroundShape();
        rectTopLineShape = new BackgroundShape();
        rectRightLineShape = new BackgroundShape();
        rectBottomLineShape = new BackgroundShape();
        rectLeftLineDrawable = new ShapeDrawable(rectLeftLineShape);
        rectTopLineDrawable = new ShapeDrawable(rectTopLineShape);
        rectRightLineDrawable = new ShapeDrawable(rectRightLineShape);
        rectBottomLineDrawable = new ShapeDrawable(rectBottomLineShape);

        lineColors = new int[]{MainApplication.getInstance().getResources().getColor(R.color.board_item_border_color),
                MainApplication.getInstance().getResources().getColor(R.color.board_item_fade_to_color)};

        rectLeftLineShape.setPosition(BackgroundShape.POSITION.LEFT);
        rectTopLineShape.setPosition(BackgroundShape.POSITION.TOP);
        rectRightLineShape.setPosition(BackgroundShape.POSITION.RIGHT);
        rectBottomLineShape.setPosition(BackgroundShape.POSITION.BOTTOM);
    }

    @Override
    public void draw(Canvas canvas) {
        int height = getBounds().height();
        int width = getBounds().width();

        rectLeftLineDrawable.setBounds(0, 0, width, height);
        rectTopLineDrawable.setBounds(0, 0, width, height);
        rectRightLineDrawable.setBounds(0, 0, width, height);
        rectBottomLineDrawable.setBounds(0, 0, width, height);

        rectLeftLineShape.setLineHeight(15 * MainApplication.RESDENSITY);
        rectLeftLineShape.setGradientColors(lineColors);
        rectLeftLineShape.setCornerRadius(0);

        rectTopLineShape.setLineHeight(15 * MainApplication.RESDENSITY);
        rectTopLineShape.setGradientColors(lineColors);
        rectTopLineShape.setCornerRadius(0);

        rectRightLineShape.setLineHeight(15 * MainApplication.RESDENSITY);
        rectRightLineShape.setGradientColors(lineColors);
        rectRightLineShape.setCornerRadius(0);

        rectBottomLineShape.setLineHeight(15 * MainApplication.RESDENSITY);
        rectBottomLineShape.setGradientColors(lineColors);
        rectBottomLineShape.setCornerRadius(0);

        rectLeftLineDrawable.draw(canvas);
        rectTopLineDrawable.draw(canvas);
        rectRightLineDrawable.draw(canvas);
        rectBottomLineDrawable.draw(canvas);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return 255;
    }
}