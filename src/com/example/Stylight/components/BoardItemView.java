package com.example.Stylight.components;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.Stylight.MainApplication;
import com.example.Stylight.R;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 27.05.13
 * Time: 14:26
 */
public class BoardItemView extends ViewGroup {

    private final ImageView imageView;
    private final TextView textView;
    private final ImageView innerShadowView;

    public BoardItemView(Context context) {
        super(context);

        imageView = new ImageView(context);
        textView = new TextView(context);
        innerShadowView = new ImageView(context);
    }

    public void init() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        addView(imageView, layoutParams);

        textView.setTextSize(12 * MainApplication.RESDENSITY);
        textView.setTextColor(MainApplication.getInstance().getResources().getColor(R.color.board_item_text_color));
        textView.setGravity(Gravity.CENTER);
        textView.setTypeface(null, Typeface.ITALIC);
        addView(textView, layoutParams);
    }

    public void setProductView() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        BoardItemInnerShadow boardItemInnerShadow = new BoardItemInnerShadow();
        boardItemInnerShadow.init();
        innerShadowView.setImageDrawable(boardItemInnerShadow);
        addView(innerShadowView, layoutParams);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setLabel(String value) {
        textView.setText(value);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = super.getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                final int childLeft = getPaddingLeft();
                final int childTop = getPaddingTop();
                child.layout(childLeft, childTop, childLeft + child.getMeasuredWidth(),
                        childTop + child.getMeasuredHeight());

            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int count = getChildCount();
        int maxHeight = 0;
        int maxWidth = 0;
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
            }
        }
        maxWidth += getPaddingLeft() + getPaddingRight();
        maxHeight += getPaddingTop() + getPaddingBottom();
        Drawable drawable = getBackground();
        if (drawable != null) {
            maxHeight = Math.max(maxHeight, drawable.getMinimumHeight());
            maxWidth = Math.max(maxWidth, drawable.getMinimumWidth());
        }
        setMeasuredDimension(resolveSize(maxWidth, widthMeasureSpec), resolveSize(maxHeight, heightMeasureSpec));
    }
}