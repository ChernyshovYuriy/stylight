package com.example.Stylight.components;

import android.graphics.*;
import android.graphics.drawable.shapes.Shape;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/25/13
 * Time: 10:33 AM
 */
public class BackgroundShape extends Shape {

    public static enum POSITION {
        TOP,
        LEFT,
        RIGHT,
        BOTTOM
    }

    private int[] gradientColors;
    private int shadowColor;
    private float mLineHeight = -1f;
    private boolean mIsShadow;
    private float mCornerRadius;
    private POSITION mPosition;

    public BackgroundShape() {
        super();
        mPosition = POSITION.TOP;
    }

    public void setPosition(POSITION mPosition) {
        this.mPosition = mPosition;
    }

    public void setLineHeight(float topLineHeight) {
        this.mLineHeight = topLineHeight;
    }

    public void setIsShadow(boolean mIsShadow) {
        this.mIsShadow = mIsShadow;
    }

    public void setShadowColor(int shadowColor) {
        this.shadowColor = shadowColor;
    }

    public void setCornerRadius(float mCornerRadius) {
        this.mCornerRadius = mCornerRadius;
    }

    public void setGradientColors(int[] gradientColors) {
        this.gradientColors = gradientColors;
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(Boolean.TRUE);

        if (mLineHeight == -1f) {
            mLineHeight = getHeight();
        }

        if (mIsShadow) {
            paint.setShadowLayer(2.5f, 0, 3, shadowColor);
        } else {
            switch (mPosition) {
                case LEFT:
                    paint.setShader(new LinearGradient(0, getHeight() / 2, mLineHeight, getHeight() / 2, gradientColors[0], gradientColors[1], Shader.TileMode.CLAMP));
                    break;
                case TOP:
                    paint.setShader(new LinearGradient(0, 0, 0, mLineHeight, gradientColors[0], gradientColors[1], Shader.TileMode.CLAMP));
                    break;
                case RIGHT:
                    paint.setShader(new LinearGradient(getWidth() - mLineHeight, getHeight() / 2, getWidth(), getHeight() / 2, gradientColors[1], gradientColors[0], Shader.TileMode.CLAMP));
                    break;
                case BOTTOM:
                    paint.setShader(new LinearGradient(0, getHeight() - mLineHeight, 0, getHeight(), gradientColors[1], gradientColors[0], Shader.TileMode.CLAMP));
                    break;
            }
        }

        RectF inset = new RectF((mCornerRadius / 2), 0, getWidth() - (mCornerRadius / 2), getHeight() - (mCornerRadius / 2));

        canvas.drawRoundRect(inset, mCornerRadius, mCornerRadius, paint);
    }
}