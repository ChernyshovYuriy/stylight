package com.example.Stylight.model;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/26/13
 * Time: 8:00 PM
 */
public class ItemData {

    private String imagePath;
    private int positionX;
    private int positionY;
    private int sizeX;
    private int sizeY;
    private boolean isProduct;
    private boolean isContent;
    private String labelText;

    public String getLabelText() {
        return labelText;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }

    public boolean isProduct() {
        return isProduct;
    }

    public void setProduct(boolean product) {
        isProduct = product;
    }

    public boolean isContent() {
        return isContent;
    }

    public void setContent(boolean content) {
        isContent = content;
    }

    public String getImagePath() {
        return imagePath == null ? "" : imagePath.endsWith("null") ? "" : imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getPositionX() {
        return positionX >= 0 ? positionX - 1 : 0;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY >= 0 ? positionY - 1 : 0;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }
}