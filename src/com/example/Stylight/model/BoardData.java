package com.example.Stylight.model;

import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 6/5/13
 * Time: 4:35 PM
 */
public class BoardData {

    JSONObject jsonObject;

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}