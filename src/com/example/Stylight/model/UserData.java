package com.example.Stylight.model;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/26/13
 * Time: 11:20 AM
 */
public class UserData {

    private String avatarURL;
    private String username;
    private String additionalInfo;

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = "http:" + avatarURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}